b = 99

while b != 1:
    print(b,"bottles of beer on the wall")
    print(b,"bottles of beer")
    b -= 1
    print("Take one down, pass it around")
    print(b,"bottles of beer on the wall")
else:
    print('No more bottles of beer on the wall, no more bottles of beer.')
    print("We've taken them down and passed them around; now we're drunk and passed out!")